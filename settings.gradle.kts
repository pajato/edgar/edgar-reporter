/*
 * This file was modified from a fragile IntelliJ created template to
 * mimic a file created by `gradle init`.
 */
pluginManagement {
    repositories {
        //maven(url = "http://dl.bintray.com/kotlin/kotlin-eap")
        // mavenLocal()
        gradlePluginPortal()
    }
    resolutionStrategy {
        eachPlugin {
            if (requested.id.id == "org.jetbrains.kotlin.jvm") {
                useModule("org.jetbrains.kotlin:kotlin-gradle-plugin:${requested.version}")
            }
            if (requested.id.id == "kotlinx-serialization") {
                useModule("org.jetbrains.kotlin:kotlin-serialization:${requested.version}")
            }
        }
    }
}

rootProject.name = "edgar-report-generator"

