plugins {
    kotlin("jvm") version "1.4.10"
    id("maven-publish")
    jacoco
}

jacoco {
    toolVersion = "0.8.5"
    reportsDir = file("$buildDir/coverage")
}

tasks.test {
    finalizedBy(tasks.jacocoTestReport) // report is always generated after tests run
}

tasks.jacocoTestReport {
    dependsOn(tasks.test) // tests are required to run before generating the report
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "com.pajato.edgar"
            artifactId = "edgar-report-generator"
            version = "1.0-SNAPSHOT"

            from(components["kotlin"])
        }
    }
}

repositories {
    mavenCentral()
    jcenter()
    mavenLocal()
}

dependencies {
    // Align versions of all Kotlin components
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))

    implementation(kotlin("stdlib-jdk8"))

    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
}

tasks {
    compileKotlin { kotlinOptions.jvmTarget = "1.8" }
    compileTestKotlin { kotlinOptions.jvmTarget = "1.8" }
}
