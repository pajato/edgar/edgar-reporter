# The Edgar application report generator

This project provides a report generator to handle two use cases: a typical code coverage report showing a high level picture of a projects code coverage and a shortened form that highlights just the elements of a project that are not covered by any tests, benchmarks, or sample runs.

## Use cases

tbc

## Structure

tbc

## Building

Install the deployable (edgar-core-<version>.jar to the local maven repoository using the Gradle task: *publishToMavenLocal* where it will be accessed by Edgar sub-projects which depend on the core.
